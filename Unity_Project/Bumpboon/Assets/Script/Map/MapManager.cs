using System;
using System.Collections;
using System.Collections.Generic;
<<<<<<< HEAD
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

=======
using UnityEngine;
using Random = UnityEngine.Random;

namespace Script.Map
{
    
>>>>>>> FightPrototype
    public class MapManager : MonoBehaviour
    {
        public MapData data;

<<<<<<< HEAD
        [Header("Map Settings")] 
        public float xDistance=1.5f;
        public float yDistance=1.5f;
        public int mapSize = 10;
        public int layerSize = 5;
        
        public GameObject itemPrefab;
        public GameObject linePrefab;
        public GameObject _mapParent;
        public List<MapTypeVisual> mapVisual = new List<MapTypeVisual>();
         Dictionary<MapItem.type, Sprite> mapSprite;


        public static MapManager i;
        // Start is called before the first frame update
        private void Awake()
        {
            i = this;
        }

        void Start()
        {
            ConvertMapTypeVisual();
            data = new MapData();
            data.GenerateList(mapSize,layerSize);
=======
        public GameObject itemPrefab;
        // Start is called before the first frame update
        void Start()
        {
            data = new MapData();
            data.GenerateList();
>>>>>>> FightPrototype
            InstantiateMap();
        }

        // Update is called once per frame
        void Update()
        {
        
        }

<<<<<<< HEAD
        public void ConvertMapTypeVisual()
        {
            mapSprite = new Dictionary<MapItem.type, Sprite>();
            for (int j = 0; j < mapVisual.Count; j++)
            {
                if (mapSprite.ContainsKey(mapVisual[j].type))
                {
                    mapSprite[mapVisual[j].type] = mapVisual[j].sprite;
                    Debug.LogWarning($"Map Visual already contains {mapVisual[j].type} replacing sprite");
                }
                else
                    mapSprite.Add(mapVisual[j].type,mapVisual[j].sprite);
            }
        }
        public void InstantiateMap()
        {
            //Create the parent gameObject
            if (!_mapParent)
            {
                _mapParent = new GameObject
                {
                    transform =
                    {
                        parent = transform
                    },
                    name = "MapParent"
                };
            }
            for (var i = 0; i < data.Layer.Count; i++)
            {
                var layer = data.Layer[i];
                
                //Create the layer gameObject
                var layObj = new GameObject();
                layObj.transform.parent = _mapParent.transform;
                layObj.transform.localPosition = new Vector2(i * xDistance, 0);
                layObj.name = $"MapLayer{i}";
                layer.listObj = layObj;
                //initialize Node
                var nodes = layer.Items;
                var count = nodes.Length;
                for (var j = 0; j < nodes.Length; j++)
                {
                    var nodeData = nodes[j];
                    if (nodeData != null)
                    {
                        if (nodeData.ItemType != MapItem.type.None)
                        {
                            //Create the node gameObject
                            var obj = Instantiate(itemPrefab);
                            MapNode node;
                            if(obj.TryGetComponent(out node))
                            {
                                obj.transform.parent = layObj.transform;
                                obj.name = $"MapItem{i}_{j}";
                                obj.transform.localPosition = new Vector2(0, -(count/2f*yDistance)+j*yDistance);
                                node.data = nodeData;
                                nodeData.node = node;
                                node.Initialize();
                            }
                            else
                            {
                                Destroy(obj);
                            }
                        }
                    }
                }
            }
            //post initialize
            for (int y = 0; y < data.Layer.Count; y++)
            {
                for (int x = 0; x < data.Layer[y].Items.Length; x++)
                {
                    var item = data.Layer[y].Items[x];
                    if (item != null)
                    {
                            
                        if (item.node)
                        {
                            item.node.RenderLine();
                        }
                    }
                }
            }
        }
        //FUNCTIONS
        public Sprite GetVisualSprite(MapItem.type type)
        {
            if (mapSprite.ContainsKey(type))
            {
                return mapSprite[type];
            }else
            {
                return null;
            }
        }
    }




[Serializable]
public class MapTypeVisual
{
    public MapItem.type type;
    public Sprite sprite;
=======
        public void InstantiateMap()
        {
            for (var index = 0; index < data.Layer.Count; index++)
            {
                var items = data.Layer[index].Items;
                for (var j = 0; j < items.Count; j++)
                {
                    var layer = data.Layer[index];
                    var obj = Instantiate(itemPrefab);
                    obj.transform.position = new Vector2(index * 1.5f - 10, j*1.5f);
                }
            }
        }
    }
    
    
>>>>>>> FightPrototype
}