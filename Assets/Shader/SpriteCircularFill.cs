using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteCircularFill : MonoBehaviour
{
    private Material m_Material = null;

    // Start is called before the first frame update
    void Awake()
    {
        m_Material = GetComponent<Renderer>().material;
    }

    public void ChangeFillAmount(int cur, int max)
    {
        float amount = max != 0 ? (cur / (float)max) * 360.0f : 0;


        if (m_Material)
        {
            m_Material.SetFloat("_Arc1", 360.0f - amount);
        }
    }
}