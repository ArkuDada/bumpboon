using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabRef : MonoBehaviour
{
    private static PrefabRef _instance;
    public static PrefabRef Instance => _instance;

    private void Awake()
    {
        _instance = this;
    }

    public GameObject BumperUI;
}