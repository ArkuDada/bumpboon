using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class TitleScreen : MonoBehaviour
{
    public const string VerStr = "V1.0 Alpha";
    public GameObject exitButton;
    public TMP_Text verText;
    public VideoPlayer player;
    private void Awake()
    {
        #if UNITY_STANDALONE
        exitButton.SetActive(true);
        #else
        exitButton.SetActive(false);
        player.source = VideoSource.Url;
        player.url = System.IO.Path.Combine (Application.streamingAssetsPath,"Monkemovemoveloop-1.webm"); 
        #endif
        verText.text = VerStr;
    }

    public void Play()
    {
        SceneManager.LoadScene("GameManager");
    }
    public void Exit()
    {
        Application.Quit();
    }
    
}
