using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPlayerHealth : MonoBehaviour
{
    private HealthUI health;
    private Health hp;

    void Start()
    {
        health = GetComponent<HealthUI>();
        hp = PlayerManager.Instance.syncHealth();
    }

    // Update is called once per frame
    void Update()
    {
        health.UpdateHealth(hp.Hp,hp.HpMax);
        health.UpdateArmor(0);
    }
}