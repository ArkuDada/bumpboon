using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MapManager : MonoBehaviour
{
    public MapData data
    {
        get
        {
            if (PlayerMap.i)
            {
                return PlayerMap.i.data;
            }
            else
            {
                return null;
            }
        }
    }

    public List<MapListObject> mapObject;
    [Header("Map Settings")] public float xDistance = 1.5f;
    public float yDistance = 1.5f;
    public float xOffset;
    public float yOffset;

    public float initNodeOffset;
    public float yChange;

    [Header("Node Settings")] public Color previousCol;
    public Color playedCol;
    public Color activeCol;
    public Color futureCol;
    [Header("Prefabs")] public GameObject itemPrefab;
    public GameObject linePrefab;
    public GameObject _mapParent;
    public GameObject mapUI;
    [Header("References")] 
    public GameObject fightUI;
    public MonkeyNode monkeyNode;
    public List<MapTypeVisual> mapVisual = new List<MapTypeVisual>();
    Dictionary<MapItem.type, Sprite> mapSprite;

    public bool mapDisable;
    public static MapManager i;

    // Start is called before the first frame update
    private void Awake()
    {
        i = this;
        ConvertMapTypeVisual();
    }

    void Start()
    {
        ToggleVisible(true);
        RandomMap();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ConvertMapTypeVisual()
    {
        mapSprite = new Dictionary<MapItem.type, Sprite>();
        for (int j = 0; j < mapVisual.Count; j++)
        {
            if (mapSprite.ContainsKey(mapVisual[j].type))
            {
                mapSprite[mapVisual[j].type] = mapVisual[j].sprite;
                Debug.LogWarning($"Map Visual already contains {mapVisual[j].type} replacing sprite");
            }
            else
                mapSprite.Add(mapVisual[j].type, mapVisual[j].sprite);
        }
    }

    public void InstantiateMap()
    {
        //Create the parent gameObject
        if (!_mapParent)
        {
            _mapParent = new GameObject
            {
                transform =
                {
                    parent = transform
                },
                name = "MapParent"
            };
        }

        System.Random ran = new System.Random(PlayerMap.i.seed);
        mapObject = new List<MapListObject>();
        for (var i = 0; i < data.layer.Count; i++)
        {
            var layer = data.layer[i];


            var nodes = layer.items;
            var nodesLength = nodes.Length;

            //Create the layer gameObject
            var layObj = new GameObject();
            layObj.transform.parent = _mapParent.transform;
            layObj.transform.localPosition = new Vector2((i+initNodeOffset) * xDistance * layObj.transform.localScale.x, yChange);
            layObj.name = $"MapLayer{i}";

            mapObject.Insert(i, new MapListObject(i, layObj, nodesLength));
            //initialize Node
            for (var j = 0; j < nodesLength; j++)
            {
                var nodeData = nodes[j];
                if (nodeData != null)
                {
                    if (nodeData.ItemType != MapItem.type.None)
                    {
                        //Create the node gameObject
                        var obj = Instantiate(itemPrefab);
                        MapNode node;
                        if (obj.TryGetComponent(out node))
                        {
                            obj.transform.parent = layObj.transform;
                            obj.name = $"MapItem{i}_{j}";
                            float ranX = ran.Next(0, 1000) / 1000f;
                            float ranY = ran.Next(0, 1000) / 1000f;
                            obj.transform.localPosition =
                                new Vector2(0, -((nodesLength - 1) / 2f * yDistance) + j * yDistance) +
                                new Vector2(ranX * xOffset, ranY * yOffset);
                            node.nodePos = nodeData.itemPos;
                            mapObject[i].node[j] = node;
                            node.Initialize();
                        }
                        else
                        {
                            Destroy(obj);
                        }
                    }
                }
            }
        }

        //post initialize
        for (int x = 0; x < mapObject.Count; x++)
        {
            for (int y = 0; y < mapObject[x].node.Length; y++)
            {
                var node = mapObject[x].node[y];
                if (node)
                {
                    node.RenderLine();
                }
            }
        }

        UpdateMap();
    }
[Button]
    public void UpdateMap()
    {
        for (int x = 0; x < mapObject.Count; x++)
        {
            for (int y = 0; y < mapObject[x].node.Length; y++)
            {
                var node = mapObject[x].node[y];
                if (node != null)
                {
                    Color nodeCol = Color.white;
                    switch (node.data.playerState)
                    {
                        case MapItem.State.Previous:
                            nodeCol = previousCol;
                            break;
                        case MapItem.State.Played:
                            nodeCol = playedCol;
                            break;
                        case MapItem.State.Active:
                            nodeCol = activeCol;
                            break;
                        case MapItem.State.Future:
                            nodeCol = futureCol;
                            break;
                    }

                    node.visual.color = nodeCol;
                    
                    if (node.button)
                    {
                        node.button.interactable = node.data.playerState == MapItem.State.Active;
                    }
                }

                
            }
        }
    }

    public void DestroyMap()
    {
        if (data == null) return;
        for (var i = 0; i < mapObject.Count; i++)
        {
            var obj = mapObject[i].listObj;
            if (obj)
            {
                Destroy(obj);
            }
        }
    }

    [Button]
    public void ResetMap()
    {
        DestroyMap();
        PlayerMap.i.GenerateMap();
        LoadMap();
    }

    public void LoadMap()
    {
        InstantiateMap();
    }

    [Button]
    public void RandomMap()
    {
        PlayerMap.i.seed = UnityEngine.Random.Range(0, int.MaxValue);
        ResetMap();
    }

    //FUNCTIONS
    public Sprite GetVisualSprite(MapItem.type type)
    {
        if (mapSprite.ContainsKey(type))
        {
            return mapSprite[type];
        }
        else
        {
            return null;
        }
    }

    public void ToggleVisible(bool state)
    {
        if(fightUI)fightUI.SetActive(!state);
        mapUI.SetActive(state);
    }
}

[Serializable]
public class MapListObject
{
    public int listPos;
    public GameObject listObj;
    public MapNode[] node;

    public MapListObject(int l, GameObject obj, int nCount)
    {
        listPos = l;
        listObj = obj;
        node = new MapNode[nCount];
    }
}


[Serializable]
public class MapTypeVisual
{
    public MapItem.type type;
    public Sprite sprite;
}