using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMap : MonoBehaviour
{
    public static PlayerMap i;
    public MapData data;
    public int activeLayer;
    public int seed = 69420;
    public MapGenerator generator;
    public int mapSize = 10;
    public int layerSize = 5;
    
    private void Awake()
    {
        if(i) Destroy(gameObject);
        i = this;
    }

    private void Start()
    {
        GenerateMap();
    }

    public void GenerateMap()
    {
        data = new MapData(seed,generator);
        data.GenerateList(mapSize,layerSize);
        activeLayer = 0;
    }

    public MapItem GetItem(Vector2Int pos)
    {
        if (pos.x < data.layer.Count)
        {
            if (pos.y < data.layer[pos.x].items.Length)
            {
                return data.layer[pos.x].items[pos.y];
            }
        } 
        Debug.LogError($"MapItem not found at {pos}");
        return new MapItem();
    }
    public void CompleteNode(Vector2Int nodePos)
    {
        //FightManager.Instance.OnFightEnd.RemoveAllListeners();
        Debug.Log($"Node complete {nodePos}");
        int x = nodePos.x;
        var curNode = data.layer[nodePos.x].items[nodePos.y];
        for (int y = 0; y < data.layer[x].items.Length; y++)
        { 
            var item = data.layer[x].items[y];
            if (item != null)
            {
                //Debug.Log($"Previous node at {x},{y}");
                item.playerState = MapItem.State.Previous;
                
            }
        }
        //Debug.Log($"Played node at {nodePos.x},{nodePos.y}");
        curNode.playerState = MapItem.State.Played;
        for (int j = 0; j < curNode.Connection.Count; j++)
        {
            var connectNodePos = curNode.Connection[j];
            var connectNode = data.layer[connectNodePos.x].items[connectNodePos.y];
            if (connectNode != null)
            {
                //Debug.Log($"Active node at {connectNodePos.x},{connectNodePos.y}");
                connectNode.playerState = MapItem.State.Active;
            }
        }

        activeLayer = x + 1;
        if (MapManager.i)
        {
            MapManager.i.ToggleVisible(true);
            MapManager.i.mapDisable = false;
            MapManager.i.UpdateMap();
        }
        else Debug.LogWarning("MapManager Not Found");
        

    }
}

