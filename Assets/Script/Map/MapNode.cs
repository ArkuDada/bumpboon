using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MapNode : MonoBehaviour
{
    public Vector2Int nodePos;
    public MapItem data => PlayerMap.i.GetItem(nodePos);
    public Image visual;
    public Button button;
    public void Initialize()
    {
        if (data!=null)
        {
            if (data.ItemType == MapItem.type.None)
            {
                Destroy(gameObject);
            }

            visual.sprite = MapManager.i.GetVisualSprite(data.ItemType);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Pressed()
    {
        var item = data;
        if(item.playerState!=MapItem.State.Active) return;
        if (MapManager.i)
        {
            if(MapManager.i.mapDisable) return;
            MapManager.i.mapDisable = true;
            
            if(MapManager.i.monkeyNode) MapManager.i.monkeyNode.WalkTowards(this); 
        }
    }

    public void NodeAction()
    {
        var item = data;
        
        switch (item.ItemType)
        {
            case MapItem.type.Minion:
            {
                if (item.enDetail)
                {
                    FightSceneLoader.Instance.LoadFight(item.enDetail);
                    FightManager.Instance.mapCallback.SetNode(nodePos);
                }
                return;
            }
            case MapItem.type.Chest:
            {
                PlayerUIManager.Instance.gameEndUI.gameObject.SetActive(true);
                PlayerUIManager.Instance.gameEndUI.Play("GameOver");
                return;
            }
            case MapItem.type.Random:
            {
                PlayerUIManager.Instance.ToggleRandomUI(true);
                if(PlayerMap.i) PlayerMap.i.CompleteNode(data.itemPos);
                PlayerManager.Instance.Heal(40);
                return;
            }
            default:
            {
                if(PlayerMap.i) PlayerMap.i.CompleteNode(data.itemPos);
                return;
            }
                
        }
    }
    public void RenderLine()
    {
        data.Connection = data.Connection.Distinct().ToList();
        var connection = data.Connection;
        List<Vector2Int> toRemove = new List<Vector2Int>();
        for (int i = 0; i < connection.Count; i++)
        {
            var con = connection[i];
            if (MapManager.i.mapObject.Count > con.x)
            {
                if (MapManager.i.mapObject[con.x].node.Length > con.y)
                {
                    if (MapManager.i.mapObject[con.x].node[con.y]!=null)
                    {
                        MapNode nodeConnect = 
                            MapManager.i.mapObject[con.x].node[con.y];
                        if (nodeConnect.gameObject)
                        {
                            var line = Instantiate(MapManager.i.linePrefab,transform);
                            LineRenderer render;
                            if (line.TryGetComponent(out render))
                            {
                                render.SetPosition(1,nodeConnect.transform.position-gameObject.transform.position);
                                continue;
                            }
                            else
                            {
                                Destroy(line);
                            }
                        }
                    }
                }
            }
            //delete connection if non found
            toRemove.Add(con);
            //Debug.LogWarning($"Connection {con} not found");
        }

        foreach (var re in toRemove)
        {
            connection.Remove(re);
        }
    }
}
