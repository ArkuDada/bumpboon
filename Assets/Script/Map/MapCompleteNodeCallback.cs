using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCompleteNodeCallback : MonoBehaviour
{
    private Vector2Int toNode;

    public void SetNode(Vector2Int node)
    {
        toNode = node;
    }
    public void Complete()
    {
        if(PlayerMap.i) PlayerMap.i.CompleteNode(toNode);
        toNode = Vector2Int.zero;
    }
}
