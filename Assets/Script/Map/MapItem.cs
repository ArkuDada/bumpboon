using System;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
    public class MapItem
    {
        public Vector2Int itemPos;
        public enum type
        {
            None,
            Default,
            Minion,
            Random,
            Chest,
            Shop,
            Boss
        }
        public enum PostChainGen
        {
            Connect,
            NewNode,
        }
        public enum State
        {
            Previous,
            Played,
            Active,
            Future
        }
        public List<KeyValuePair<PostChainGen,int>> postChainGen = new List<KeyValuePair<PostChainGen, int>>();
        public List<Vector2Int> Connection = new List<Vector2Int>();
        public type ItemType;
        //public MapNode node;
        public State playerState;
        public EnemyDetail enDetail;

        public MapItem(Vector2Int p,type item)
        {
            itemPos = p;
            ItemType = item;
        }
        public MapItem()
        {
            itemPos = new Vector2Int(0,0);
            ItemType = type.Default;
        }
    }

