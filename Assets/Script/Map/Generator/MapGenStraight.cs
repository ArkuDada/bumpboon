using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "StraightGenerator", menuName = "Map/Generator/Straight", order = 1)]
public class MapGenStraight : MapGenerator
{
    
    public override List<MapList> GenerateList(int mapSize, int layerSize, int seed)
        {
            var Layer = new List<MapList>();
            System.Random ran = new System.Random(seed);
            
            
            for (int x = 0; x < mapSize; x++)
            {
                var l = new MapList(layerSize);
                
                for (int y = 0; y < layerSize; y++)
                {
                    l.items[y] = GetItem(new Vector2Int(x,y));
                }
                Layer.Add(l);
            }
            
            return Layer;
        }

    public  virtual MapItem GetItem(Vector2Int pos)
        {
            switch (pos.y)
            {
                case 2:
                    var i = new MapItem(pos,MapItem.type.Minion);
                    i.Connection.Add( new Vector2Int(pos.x+1,pos.y));
                    return i;
                default:
                    
                    var j = new MapItem(pos,MapItem.type.Default);
                    j.Connection.Add( new Vector2Int(pos.x+1,pos.y));

                    return j;
            }
        }
}
