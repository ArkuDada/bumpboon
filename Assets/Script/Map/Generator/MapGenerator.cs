using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultGenerator", menuName = "Map/Generator/Default", order = 1)]
public class MapGenerator : ScriptableObject
{
    public int chainCount = 3;
    public float moveChances = 0.3f;
    public bool disableConnectChain;
    public bool disableFixCrossing;
    public float connectChances = 0.1f;
    public List<LayerGen> layerGens;

    public virtual List<MapList> GenerateList(int mapSize, int layerSize, int seed)
    {
        var Layer = new List<MapList>();
        System.Random ran = new System.Random(seed);


        for (int x = 0; x < mapSize; x++)
        {
            var l = new MapList(layerSize);

            for (int y = 0; y < layerSize; y++)
            {
            }

            Layer.Add(l);
        }

        var chainToGen = RandomNumberList(layerSize, chainCount, seed);
        for (int i = 0; i < chainToGen.Count; i++)
        {
            GenerateChain(chainToGen[i] - 1, Layer, seed);
        }

        PostGenerateChain(Layer, seed);
        FixChainCrossing(Layer, seed);
        return Layer;
    }

    //generate a chain of nodes
    public void GenerateChain(int startingPos, List<MapList> Layer, int seed)
    {
        System.Random ran = new System.Random(seed * startingPos);
        //List<Vector2> extraNodes;

        int spawnPos = startingPos;
        for (int x = 0; x < Layer.Count; x++)
        {
            //generate the node
            int currentPos = spawnPos;
            if (x < layerGens.Count)
            {
                if (layerGens[x].nodeMerge)
                {
                    currentPos = Mathf.RoundToInt( (Layer[x].items.Length-1) / 2f);
                }
            }

            int itemSeed = ran.Next();
            if (Layer[x].items[currentPos] == null)
            {
                Layer[x].items[currentPos] = GetItem(new Vector2Int(x, currentPos), itemSeed);
            }

            //randomize the next node location
            float ranN = ran.Next(0, 1000) / 1000f;
            if (ranN < moveChances)
            {
                if (spawnPos == 0)
                {
                    spawnPos += 1;
                }
                else if (spawnPos >= Layer[x].items.Length - 1)
                {
                    spawnPos -= 1;
                }
                else
                {
                    if (ranN < moveChances / 2f) spawnPos -= 1;
                    else spawnPos += 1;
                }
            }

            float ranC = ran.Next(0, 1000) / 1000f;
            if (ranC < connectChances)
            {
                int direction = 0;

                if (currentPos == 0)
                {
                    direction = 1;
                }
                else if (currentPos >= Layer[x].items.Length - 1)
                {
                    direction = -1;
                }
                else
                {
                    if (ranC < connectChances / 2f) direction = -1;
                    else direction = 1;
                }

                var kp = new KeyValuePair<MapItem.PostChainGen, int>(MapItem.PostChainGen.Connect, direction);
                //Debug.Log(kp);
                Layer[x].items[currentPos].postChainGen.Add(kp);
            }

            var nextPos = spawnPos;
            if (x+1 < layerGens.Count)
            {
                if (layerGens[x+1].nodeMerge)
                {
                    nextPos = Mathf.RoundToInt( (Layer[x+1].items.Length-1) / 2f);
                }
            }
            Layer[x].items[currentPos].Connection.Add(new Vector2Int(x + 1, nextPos));
        }
    }

    //generate all connection between nodes
    void PostGenerateChain(List<MapList> Layer, int seed)
    {
        for (int x = 0; x < Layer.Count; x++)
        {
            var l = Layer[x];

            for (int y = 0; y < l.items.Length; y++)
            {
                if (l.items[y] != null)
                {
                    for (int i = 0; i < l.items[y].postChainGen.Count; i++)
                    {
                        //Debug.Log($"PGC @({x},{y}) {l.Items[y].postChainGen[i].Key} {l.Items[y].postChainGen[i].Value}");
                        var genType = l.items[y].postChainGen[i].Key;
                        var genDirection = l.items[y].postChainGen[i].Value;

                        switch (genType)
                        {
                            case MapItem.PostChainGen.Connect:
                                if (disableConnectChain) break;
                                if (x + 1 < Layer.Count)
                                {
                                    if (genDirection == 0) continue; //haha no forever while loop
                                    int check = y;
                                    while (check > 0 && check < Layer[x + 1].items.Length - 1)
                                    {
                                        check += genDirection;
                                        if (Layer[x + 1].items[check] != null)
                                        {
                                            var con = new Vector2Int(x + 1, check);
                                            if (!l.items[y].Connection.Contains(con))
                                            {
                                                l.items[y].Connection.Add(con);
                                                break;
                                            }
                                        }
                                    }
                                }

                                break;

                            case MapItem.PostChainGen.NewNode:
                                break;
                        }
                    }
                }
            }
        }
    }

    //fix chain crossing
    void FixChainCrossing(List<MapList> Layer, int seed)
    {
        if (disableFixCrossing) return;
        //loop all layer
        for (int x = 0; x < Layer.Count; x++)
        {
            var l = Layer[x];

            //loop each node
            for (int y = 0; y < l.items.Length; y++)
            {
                //check if there's node
                if (l.items[y] != null)
                {
                    //loop all node below
                    for (int i = y + 1; i < l.items.Length; i++)
                    {
                        if (l.items[i] != null)
                        {
                            //loop each connection
                            for (int j = 0; j < l.items[i].Connection.Count; j++)
                            {
                                for (int n = 0; n < l.items[y].Connection.Count; n++)
                                {
                                    var con = l.items[y].Connection[n];
                                    if (con.y > l.items[i].Connection[j].y)
                                    {
                                        var swap = con;
                                        l.items[y].Connection[n] = l.items[i].Connection[j];
                                        l.items[i].Connection[j] = swap;
                                        //Debug.Log($"Connection crossing @{y},{x}&{i},{x}");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    //Node Type generation
    MapItem GetItem(Vector2Int pos, int seed)
    {
        var j = new MapItem(pos,MapItem.type.Default);
        var ran = new System.Random(seed).Next(0, 1000) / 1000f;
        if (pos.x < layerGens.Count)
        {
            var na = layerGens[pos.x].NodeArray;
            var genIndex = Mathf.RoundToInt(ran * (na.Length - 1));
            j.ItemType = na[genIndex].type;
            //if(na[genIndex].enemyDetail)Debug.Log($"{na[genIndex].enemyDetail.name} {pos}");
            j.enDetail = na[genIndex].enemyDetail;

        }
        if (pos.x == 0)
            j.playerState = MapItem.State.Active;
        else
            j.playerState = MapItem.State.Future;
        return j;
    }

    List<int> RandomNumberList(int max, int count, int seed)
    {
        seed += 74534;
        System.Random rand = new System.Random(seed);
        List<int> possible = Enumerable.Range(1, max).ToList();
        List<int> listNumbers = new List<int>();
        for (int i = 0; i < count; i++)
        {
            if (possible.Count <= 0) break;
            int index = rand.Next(0, possible.Count);
            listNumbers.Add(possible[index]);
            possible.RemoveAt(index);
        }

        return listNumbers;
    }
}

[Serializable]
public class LayerGen
{
    public List<NodeChances> nodeChances;
    public bool nodeMerge;

    public NodeGenDetail[] NodeArray
    {
        get
        {
            if (_nodeArray.IsNullOrEmpty())
            {
                var tl = new List<NodeGenDetail>();
                foreach (var nc in nodeChances)
                {
                    for (int i = 0; i < nc.chances; i++)
                    {
                        tl.Add(nc.nodeGen);
                    }
                }

                _nodeArray = tl.ToArray();
            }

            return _nodeArray;
        }
    }

    private NodeGenDetail[] _nodeArray;
}

[Serializable]
public class NodeChances
{
    //public MapItem.type type;
    //public EnemyDetail enemyDetail;
    public NodeGenDetail nodeGen;
    public int chances = 1;

}
[Serializable]
public struct NodeGenDetail
{
    public MapItem.type type;
    public EnemyDetail enemyDetail;
}