using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
    public class MapData
    {
        public List<MapList> layer;
        public int seed;
        public MapGenerator generator;
        
        public void GenerateList(int mapSize, int layerSize)
        {
            if (!generator)
            {
                Debug.LogError("No map generator found!");
                return;
            }
            layer = generator.GenerateList(mapSize, layerSize, seed);
        }

        public MapData(int s, MapGenerator gen)
        {
            seed = s;
            generator = gen;
        }
    }

[Serializable]
    public class MapList
    {
        public MapItem[] items;
        //public GameObject listObj;
        public MapList(int size)
        {
            items = new MapItem[size];
        }

        
    }


