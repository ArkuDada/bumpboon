using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkeyNode : MonoBehaviour
{
    //public Vector2 walkPos;
    public MapNode node;
    public bool isWalking;

    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.SetAsLastSibling();
        if (isWalking&&node)
        {
            var walkPos = node.transform.position;
            transform.position = Vector3.MoveTowards(transform.position, walkPos, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, walkPos) < 0.1f)
            {
                isWalking = false;
                node.NodeAction();
                node = null;
                
            }
        }
    }

    public void WalkTowards(MapNode n)
    {
        isWalking = true;
        node = n;
    }
}
