using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxManager : MonoBehaviour
{
    public AudioSource source;
    
    public List<SFXPair> sfxList = new List<SFXPair>();
    Dictionary<string, AudioClip> sfxDict;

    public static SfxManager I;
    // Start is called before the first frame update
    void Awake()
    {
        ConvertDict();
    }

    private void Start()
    {
        I = this;
    }


    public void PlayClip(string id)
    {
        if (sfxDict.ContainsKey(id))
        {
            source.PlayOneShot(sfxDict[id]);
        }
        else
        {
            Debug.LogWarning($"Sfx {id} not found!");
        }
    }
    public void ConvertDict()
    {
        sfxDict = new Dictionary<string, AudioClip>();
        for (int j = 0; j < sfxList.Count; j++)
        {
            if (sfxDict.ContainsKey(sfxList[j].id))
            {
                sfxDict[sfxList[j].id] = sfxList[j].clip;
                Debug.LogWarning($"SFX already contains {sfxList[j].id} replacing clips");
            }
            else
                sfxDict.Add(sfxList[j].id, sfxList[j].clip);
        }
    }
}
[Serializable]
public class SFXPair
{
    public string id;
    public AudioClip clip;
}
