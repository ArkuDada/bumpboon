using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour
{
    [SerializeField]private Transform Camera;
    private void OnTriggerEnter2D(Collider2D other)
    {
        Camera.position = transform.position;
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        Camera.position = transform.position;
    }
}
