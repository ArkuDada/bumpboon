﻿using System;
using UnityEngine;

public class Timer
{
    private int _tickStart;
    private int _tick;
    public int RemainTick => _tick;
    
    private bool isActive;

    public bool IsActive
    {
        get => isActive;
        set => isActive = value;
    }

    public Timer(int _tick)
    {
        this.isActive = true;
        this._tick = _tickStart = _tick;
        TimeSystem.Tick += Tick;
    }

    public Timer(int _tick, bool _isActive)
    {
        this.isActive = _isActive;
        this._tick = _tickStart = _tick;
        TimeSystem.Tick += Tick;
    }

    private void Tick(object sender, EventArgs eventArgs)
    {
        if (isActive)
        {
            _tick--;
        }
    }

    public bool TimerEnd()
    {
        if (_tick == 0)
        {
            _tick = _tickStart;
            return true;
        }

        return false;
    }
}