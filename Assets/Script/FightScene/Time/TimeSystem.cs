﻿using System;
using UnityEngine;

public class TimeSystem : MonoBehaviour
{
    private static TimeSystem _instance;
    public static event EventHandler Tick;
    //[SerializeField] private bool IsTickingSerialize = true;
    private static bool IsTicking = true;

    public static bool Ticking
    {
        get => IsTicking;
        set => IsTicking = value;
    }

    [SerializeField] private float timePerTick = 1.0f;

    private float time = 0;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        time = timePerTick;
    }

    private void Update()
    {
        //IsTicking = _instance.IsTickingSerialize;

        if (IsTicking)
        {
            time -= Time.deltaTime;
            if (time <= 0)
            {
                time = 0;
                OnTimerEnd();
                time = timePerTick;
            }
        }
    }

    private static void OnTimerEnd()
    {
        Tick?.Invoke(_instance, EventArgs.Empty);
    }
}