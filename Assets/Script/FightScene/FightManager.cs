using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class FightManager : MonoBehaviour
{
    private static FightManager _instance;

    public static FightManager Instance
    {
        get => _instance;
    }

    public FightState State;

    public MapCompleteNodeCallback mapCallback;
    public UnityEvent OnFightStart;
    public UnityEvent OnFightEnd;

    private void Awake()
    {
        _instance = this;
        OnFightEnd.AddListener(FightEndEvent);
    }

    void Start()
    {
        State = FightState.Unload;
        OnFightStart.AddListener(delegate { PlayerManager.Instance.OnFightStart(); });
    }


    private bool isPause = false;

    [Button]
    public void TogglePause()
    {
        isPause = !isPause;
        if (isPause)
        {
            FightPause();
        }
        else
        {
            FightUnpause();
        }
    }

    public void FightStart()
    {
        OnFightStart.Invoke();
        State = FightState.Fighting;
        TimeSystem.Ticking = true;
    }

    public void FightPause()
    {
        TimeSystem.Ticking = false;
        BallManager.Instance?.PauseBall();
    }


    public void FightUnpause()
    {
        if (State == FightState.Fighting)
        {
            TimeSystem.Ticking = true;
            BallManager.Instance.UnpauseBall();
        }
    }

    private void FightEndEvent()
    {
        if (State == FightState.Fighting)
        {
            State = FightState.Reward;
            FightPause();
            PlayerUIManager.Instance.VictoryDisplay.OpenVictoryPanel();
            PlayerUIManager.Instance.VictoryDisplay.LoadReward();
        }
    }

    public void RewardCompleteEvent()
    {
        if (mapCallback) mapCallback.Complete();
        FightSceneLoader.Instance.UnloadFight();
    }
}

public enum FightState
{
    Unload,
    Fighting,
    Reward,
    End
}