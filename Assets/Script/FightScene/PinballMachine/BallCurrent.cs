using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCurrent : MonoBehaviour
{
    public Vector3 force;

    public Transform spriteObj;

    public float aniSpeed;
    // Start is called before the first frame update
    void Start()
    {
        startPos = spriteObj.transform.localPosition;
    }

    private Rigidbody2D ballRb;
    private void OnTriggerStay2D(Collider2D col)
    {
            if (col.TryGetComponent(out Rigidbody2D rb))
            {
                rb.AddForce(force);
            }
    }

    private Vector2 startPos;
    private void FixedUpdate()
    {
        float newPos = Mathf.Repeat(Time.time * force.x*aniSpeed, 4);
        spriteObj.transform.localPosition = startPos + newPos * Vector2.right;
    }
}
