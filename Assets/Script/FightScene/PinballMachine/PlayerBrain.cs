﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerBrain : MonoBehaviour
{
    private static PlayerBrain _instance;
    public static PlayerBrain Instance => _instance;
    private PlayerManager _playerManager;
    [SerializeField] private List<PlayerBumper> allStageBumper;
    [SerializeField] private int MaxBumper;
    [SerializeField] private int placeDelayTick;
    private Timer placeDelay = null;

    [Title("Deck")] [SerializeField] private PlayerBumperDeck drawPile = new PlayerBumperDeck();
    [SerializeField] private PlayerBumperDeck discardPile = new PlayerBumperDeck();


    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        _playerManager = PlayerManager.Instance;
        MaxBumper = _playerManager.MaxBumper;
        drawPile = _playerManager.Deck.Clone();
        drawPile.ShuffleDeck();

        placeDelay = new Timer(placeDelayTick);
    }

    private void Update()
    {
        if (placeDelay.TimerEnd())
        {
            if (drawPile.Count == 0)
            {
                Reshuffle();
            }
            else if (CheckBumperEmpty() && CheckBumperActiveLimit())
            {
                PlayerBumper emptyBumper = GetRandomEmptyBumper();
                Bumper randomEffect = drawPile.DrawBumper();

                emptyBumper.SetBumper(randomEffect);
            }

            if (drawPile.Count != 0)
            {
                PlayerUIManager.Instance.Loadout.ChangeList(drawPile.Deck);
            }

            placeDelay = new Timer(placeDelayTick);
        }
    }

    protected virtual bool CheckBumperEmpty()
    {
        foreach (var bumper in allStageBumper)
        {
            if (bumper.Bumper == null)
            {
                return true;
            }
        }

        return false;
    }

    public void BumperUsed(Bumper b)
    {
        discardPile.Add(b);
    }

    private void Reshuffle()
    {
        //drawPile.Deck = discardPile.Deck.ToList();
        drawPile = discardPile.Clone();
        discardPile.Deck.Clear();

        drawPile.ShuffleDeck();
    }

    private bool CheckBumperActiveLimit()
    {
        int count = 0;

        foreach (var bumper in allStageBumper)
        {
            if (bumper.Bumper != null)
            {
                count++;
            }
        }

        return count < MaxBumper;
    }

    private PlayerBumper GetRandomEmptyBumper()
    {
        PlayerBumper bumper = allStageBumper[Random.Range(0, allStageBumper.Count)];
        if (bumper.Bumper == null)
        {
            return bumper;
        }
        else
        {
            return GetRandomEmptyBumper();
        }
    }

    private int attackValue = 0;
    private int attackMultiplier = 1;

    public void AttackBuffer(int value)
    {
        attackValue += value;
    }

    public void AttackMultiplier(int value)
    {
        attackMultiplier = value;
        PlayerUIManager.Instance.ToggleParticle("AtkBuff",true);
    }

    public void CommenceAttack(AttackAnimEnum e)
    {
        PlayerUIManager.Instance.PlayAnimation(AttackAnimName.GetAnimName(e));
        PlayerUIManager.Instance.ToggleParticle("AtkBuff",false);
        
        Enemy.Instance.Damage(attackValue * attackMultiplier);
        attackValue = 0;
        attackMultiplier = 1;
    }
}

public enum AttackAnimEnum
{
    Attack,
    AttackStone
}

public static class AttackAnimName
{
    private static string[] animName =
    {
        "player_attack", "player_attack_stone"
    };

    public static string GetAnimName(AttackAnimEnum e)
    {
        return animName[(int)e];
    }
}