﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;


public class FlipperController : MonoBehaviour
{
    [Title("Flipper Audio")] [SerializeField]
    private AudioSource _audio;

    [SerializeField] FlipperBallDectector flipperBallDectector;

    [SerializeField] private AudioClip flipAir;
    [SerializeField] private AudioClip flipBall;

    [Title("Flipper Setting")] public float speed;
    [SerializeField] private bool left;
    private HingeJoint2D _joint;
    private JointMotor2D _motor;


    private bool InputSpace = false;
    private bool InputR = false;
    private bool InputL = false;

    private bool IsActive;

    private void Start()
    {
        _joint = GetComponent<HingeJoint2D>();
        _motor = _joint.motor;

        //_flipperInformation = _joint.attachedRigidbody.gameObject.GetComponent<FlipperInformation>();
    }

    void Update()
    {
        InputSpace = Input.GetKey(KeyCode.Space);
        InputL = Input.GetKey(KeyCode.LeftArrow);
        InputR = Input.GetKey(KeyCode.RightArrow);

        IsActive = Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.LeftArrow) ||
                   Input.GetKeyDown(KeyCode.RightArrow);

        if (IsActive)
        {
            var clipToPlay = flipperBallDectector.IsHitBall ? flipBall : flipAir;
            _audio.clip = clipToPlay;
            _audio.Play();
        }
    }

    private void FixedUpdate()
    {
        if (InputSpace || (InputL && left) ||
            (InputR && !left))
        {
            _motor.motorSpeed = left ? speed : -speed;
            _joint.motor = _motor;
        }
        else
        {
            _motor.motorSpeed = left ? -speed : speed;
            _joint.motor = _motor;
        }

        _joint.connectedBody.constraints = (_joint.limitState != JointLimitState2D.Inactive)
            ? RigidbodyConstraints2D.FreezeRotation
            : RigidbodyConstraints2D.None;
    }
}