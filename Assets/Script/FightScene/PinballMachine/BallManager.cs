using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class BallManager : MonoBehaviour
{
    private static BallManager _instance;
    public static BallManager Instance => _instance;

    [Title("Setting")] [SerializeField] private CinemachineVirtualCamera Camera;
    [SerializeField] private Transform[] respawn;
    [Title("Prefab")] [SerializeField] private GameObject ballPrefab;

    [Title("Balls")] [SerializeField] private GameObject mainBall = null;
    [SerializeField] private List<GameObject> ballList = new List<GameObject>();
    public int ballCount => ballList.Count;

    public UnityEvent ballZero;

    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        FalloutZone.Instance.BallFall += OnBallFall;
        SpawnBall(false);
    }

    private void Update()
    {
        RefocusMainBall();
    }

    private void RefocusMainBall()
    {
        if (ballCount > 0)
        {
            mainBall = ballList[0];
            foreach (GameObject ball in ballList)
            {
                if (ball.transform.position.y < mainBall.transform.position.y)
                {
                    mainBall = ball;
                }
            }

            Camera.Follow = mainBall.transform;
        }
    }

    [Button]
    public void SpawnBall(bool spawnAtMain)
    {
        GameObject newBall = spawnAtMain
            ? Instantiate(ballPrefab, mainBall.transform.position,
                Quaternion.identity, transform)
            : Instantiate(ballPrefab, respawn[Random.Range(0, respawn.Length)].position,
                Quaternion.identity, transform);

        ballList.Add(newBall);

        mainBall = newBall;
    }

    private void OnBallFall(object sender, EventArgs fallenBall)
    {
        FallenBall fb = fallenBall as FallenBall;
        GameObject discardBall = fb.ball;

        ballList.Remove(discardBall);
        if (discardBall == mainBall)
        {
            mainBall = null;
        }

        Destroy(discardBall);

        if (ballCount == 0)
        {
            SpawnBall(false);
            ballZero.Invoke();
        }
    }

    private List<BallState> savedBallState = new List<BallState>();

    public void PauseBall()
    {
        savedBallState.Clear();
        foreach (var ball in ballList)
        {
            Rigidbody2D rb = ball.GetComponent<Rigidbody2D>();
            savedBallState.Add(new BallState(rb.velocity, rb.angularVelocity));
            rb.bodyType = RigidbodyType2D.Static;
        }
    }

    public void UnpauseBall()
    {
        foreach (var ball in ballList)
        {
            Rigidbody2D rb = ball.GetComponent<Rigidbody2D>();

            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.WakeUp();

            rb.AddForce(savedBallState[0].velo * (rb.mass / Time.fixedDeltaTime), ForceMode2D.Force);
            rb.AddTorque(savedBallState[0].angle, ForceMode2D.Impulse);

            savedBallState.RemoveAt(0);
        }
    }

    private class BallState
    {
        public Vector2 velo;
        public float angle;

        public BallState(Vector2 v, float a)
        {
            velo = v;
            angle = a;
            //Debug.Log("velo is " + velo + " angle is " + angle);
        }
    }
}