﻿using System;
using UnityEngine;


public class FlipperBallDectector : MonoBehaviour
{
    public bool IsHitBall = false;

    private void OnTriggerEnter2D(Collider2D other)
    {
        IsHitBall = true;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        IsHitBall = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        IsHitBall = false;
    }
}