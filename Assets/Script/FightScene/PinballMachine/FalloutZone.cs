using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FalloutZone : MonoBehaviour
{
    private static FalloutZone _instance;
    public static FalloutZone Instance => _instance;

    public event EventHandler BallFall;

    private void Awake()
    {
        _instance = this;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        FallenBall fallenBall = new FallenBall();
        fallenBall.ball = other.gameObject;
        BallFall?.Invoke(this, fallenBall);
    }
}

public class FallenBall : EventArgs
{
    public GameObject ball;
}