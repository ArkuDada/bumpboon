using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRailing : MonoBehaviour
{
    public Vector2[] movePoint;
    public int index;
    public bool reverse;
    public float speed;

    public Vector3 objPos;
    // Start is called before the first frame update
    void Start()
    {
        objPos = transform.position;
        if(reverse)index = movePoint.Length - 1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var des = (Vector3) movePoint[index] + objPos;
        transform.position = Vector3.MoveTowards(transform.position,des , speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, des) < 0.01f)
        {
            if (reverse) index -= 1;
            else index += 1;

            if (index >= movePoint.Length)
            {
                index = 0;
            }

            if (index < 0)
            {
                index = movePoint.Length - 1;
            }
        }
    }
}
