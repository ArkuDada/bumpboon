using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceableObject : MonoBehaviour
{
    public float bouncePower = 15f;
    private void OnCollisionEnter2D(Collision2D other)
    {
        other.rigidbody.AddForce(-other.contacts[0].normal * bouncePower, ForceMode2D.Impulse);
    }
}
