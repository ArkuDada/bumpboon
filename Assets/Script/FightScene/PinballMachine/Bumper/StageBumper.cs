﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public abstract class StageBumper : MonoBehaviour
{
    private Animator _animator;
    private AudioSource _audio;
    protected Bumper _bumper = null;

    public Bumper Bumper
    {
        get => _bumper;
    }

    protected SpriteRenderer _spriteRenderer;
    [SerializeField] protected Sprite blankBumper;

    protected virtual void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _audio = GetComponent<AudioSource>();
        blankBumper = _spriteRenderer.sprite;
    }

    protected virtual void Update()
    {
    }

  
    protected virtual void OnHit()
    {
        _audio.Play();
        _animator.Play("onhit");
    }


    public virtual void SetBumper(Bumper effect)
    {
        _bumper = effect;
        _spriteRenderer.sprite = _bumper.Sprite;
    }

    public virtual void BumperClear()
    {
        _spriteRenderer.sprite = blankBumper;

        _bumper = null;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        OnHit();
    }
}