using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Bumper
{
    [SerializeField] private string ID;
    [SerializeField] private Sprite _sprite;
    public Sprite Sprite => _sprite;
    private BumperEffect _BumperEffect;
    public BumperEffect effect => _BumperEffect;

    public bool isUpgraded = false;
    public Bumper(BumperDetail bd)
    {
        ID = bd.ID;
        _sprite = bd.Sprite;
        _BumperEffect = bd.CreateBumper();
    }

    public void UpgradeBumper()
    {
        isUpgraded = true;
        _BumperEffect.Upgrade();
    }
}