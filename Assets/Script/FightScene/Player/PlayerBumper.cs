﻿using System;
using UnityEngine;


public class PlayerBumper : StageBumper
{
    protected override void Start()
    {
        base.Start();
        BallManager.Instance.ballZero.AddListener(delegate { BumperReturn(); });
    }

    protected override void OnHit()
    {
        base.OnHit();
        if (_bumper != null)
        {
            PlayerBrain.Instance.BumperUsed(_bumper);
            _bumper.effect.OnHit();
        }

        BumperClear();
    }

    private void BumperReturn()
    {
        if (_bumper != null)
        {
            PlayerBrain.Instance.BumperUsed(_bumper);
        }

        BumperClear();
    }
}