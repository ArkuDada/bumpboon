﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerManager : MonoBehaviour, IHealth
{
    public bool debugMode = false;
    [SerializeField] private PlayerBumperDeck _deck;
    public PlayerBumperDeck Deck => _deck;

    public int MaxBumper;
    
    private static PlayerManager _instantce;

    public static PlayerManager Instance
    {
        get => _instantce;
    }

    private Health Health;

    private void Awake()
    {
        _instantce = this;
        if (!debugMode)
        {
            _deck.LoadStarterDeck();
        }
        else
        {
            _deck.DebugGetAllBumper();
        }
    }

    private void Start()
    {
        Health = GetComponent<Health>();
    }

    private void Update()
    {
        if (Health.Hp == 0)
        {
            OnPlayerKilled();
        }
    }

    public void OnFightStart()
    {
        
        Health.ClearArmor();
        UpdateHealthUI();
    }

    public void OnPlayerKilled()
    {
        FightManager.Instance.FightPause();
        PlayerUIManager.Instance.gameOverUI.gameObject.SetActive(true);
        PlayerUIManager.Instance.gameOverUI.Play("GameOver");
    }
    public void Damage(int amount)
    {
        Health.Damage(amount);
        PlayerUIManager.Instance.PlayAnimation("Hurt");
        SfxManager.I.PlayClip("damage");
        UpdateHealthUI();
    }

    public void Armor(int amount)
    {
        Health.AddArmor(amount);
        SfxManager.I.PlayClip("armor");
        UpdateHealthUI();
    }

    public void Heal(int amount)
    {
        Health.Heal(amount);
        PlayerUIManager.Instance.PlayParticleOnce("Heal");
        SfxManager.I.PlayClip("heal");
        UpdateHealthUI();
    }

    public void UpdateHealthUI()
    {
        PlayerUIManager.Instance.Health.UpdateHealth(Health.Hp, Health.HpMax);
        PlayerUIManager.Instance.Health.UpdateArmor(Health.Armor);
    }
    
    public Health syncHealth()
    {
        return Health;
    }
}