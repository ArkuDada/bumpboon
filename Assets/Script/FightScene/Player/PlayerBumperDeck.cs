﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class PlayerBumperDeck
{
    [SerializeField] private List<Bumper> bumperDeck;

    public List<Bumper> Deck
    {
        get => bumperDeck;
        set => bumperDeck = value;
    }

    public int Count => bumperDeck.Count;

    public void ShuffleDeck()
    {
        List<Bumper> tempDeck = bumperDeck.ToList();
        List<Bumper> shuffleDeck = new List<Bumper>();
        for (int i = 0; i < bumperDeck.Count; i++)
        {
            int rng = Random.Range(0, tempDeck.Count);
            Bumper temp = tempDeck[rng];
            tempDeck.Remove(temp);
            shuffleDeck.Add(temp);
        }

        bumperDeck = shuffleDeck;
    }

    public Bumper DrawBumper()
    {
        if (bumperDeck.Count >= 1)
        {
            Bumper draw = bumperDeck[0];
            bumperDeck.RemoveAt(0);
            return draw;
        }
        else
        {
            return null;
        }
    }

    public PlayerBumperDeck Clone()
    {
        PlayerBumperDeck newBumperDeck = new PlayerBumperDeck();
        newBumperDeck.bumperDeck = new List<Bumper>(bumperDeck);
        return newBumperDeck;
    }

    public void Add(Bumper b)
    {
        bumperDeck.Add(b);
    }

    public void Clear()
    {
        bumperDeck.Clear();
    }

    public void LoadStarterDeck()
    {
        List<BumperDetail> bds = Database.GetStarterDeck();
        foreach (BumperDetail b in bds)
        {
            bumperDeck.Add(new Bumper(b));
        }
    }

    public void DebugGetAllBumper()
    {
        List<BumperDetail> bds = Database.GetDebugDeck();
        foreach (BumperDetail b in bds)
        {
            bumperDeck.Add(new Bumper(b));
        }
    }

    private void DebugGetRandomBumper()
    {
        bumperDeck.Add(new Bumper(Database.GetRandomBumper()));
    }
}