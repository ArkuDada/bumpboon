﻿using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private int health;
    public int Hp => health;
    [SerializeField] private int healthMax;
    public int HpMax => healthMax;
    [SerializeField] private int armor;
    public int Armor => armor;

    public void Damage(int amount)
    {
        int dmgToHealth = amount - armor;
        armor -= amount;
        
        if (dmgToHealth >= 0)
        {
            health -= dmgToHealth;
        }

        if (armor < 0)
        {
            armor = 0;
        }

        if (health < 0)
        {
            health = 0;
        }
    }

    public void Heal(int amount)
    {
        health += amount;
        if (health > healthMax)
        {
            health = healthMax;
        }
    }

    public void AddArmor(int amount)
    {
        armor += amount;
    }

    public void ClearArmor()
    {
        armor = 0;
    }
}

public interface IHealth
{
    public void Damage(int amount);
    public void Armor(int amount);
    public void Heal(int amount);
}