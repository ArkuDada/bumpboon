using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DustOnImpact : MonoBehaviour
{
    [SerializeField] private GameObject dustPE;
    public float forceToSpawn;

    private void OnCollisionEnter2D(Collision2D other)
    {
        var contact = other.GetContact(0);
        Vector3 cp = contact.point;
        float magnitude = other.relativeVelocity.magnitude;
        //Debug.Log(cp + " " + magnitude);

        if (magnitude > forceToSpawn)
        {
            Destroy(Instantiate(dustPE, cp, Quaternion.identity,ParticleHandler.Instance.transform), 1.5f);
        }
    }
}