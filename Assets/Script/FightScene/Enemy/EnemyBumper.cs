﻿using System.Collections.Generic;
using UnityEngine;


public class EnemyBumper : StageBumper
{
    private Timer attackTimer = new Timer(0, false);
    [SerializeField] private GameObject timerDisplay;
    [SerializeField] private int _attackTick;

    public int AttackTick
    {
        get => _attackTick;
        set => _attackTick = value;
    }

    [SerializeField] private int _bumperHealth;

    public int BumperHealth
    {
        get => _bumperHealth;
        set => _bumperHealth = value;
    }

    protected override void Update()
    {
        base.Update();
        
        if (attackTimer.IsActive)
        {
            timerDisplay.GetComponent<SpriteCircularFill>().ChangeFillAmount(attackTimer.RemainTick,_attackTick);
        }

        if (attackTimer.TimerEnd())
        {
            _bumper?.effect.Activate();
            attackTimer = new Timer(_attackTick);
        }
    }

    public override void SetBumper(Bumper effect)
    {
        EnemyBumper bumper = this;
        base.SetBumper(effect);

        effect.effect.OnApplied(bumper);

        attackTimer = new Timer(_attackTick);
        timerDisplay.SetActive(attackTimer.IsActive);
    }

    protected override void OnHit()
    {
        base.OnHit();
        _bumperHealth--;
        if (_bumperHealth == 0)
        {
            BumperClear();
        }
    }

    public void SetTimerStatus(bool flag)
    {
        attackTimer.IsActive = flag;
    }

    public override void BumperClear()
    {
        base.BumperClear();
        attackTimer = new Timer(0, false);
        timerDisplay.SetActive(attackTimer.IsActive);
    }
}