﻿using UnityEngine;


public class EnemyTest : Enemy
{

    protected override void Attack()
    {
        EnemyBumper randomBumper = GetRandomEmptyBumper();
        if (randomBumper.Bumper == null)
        {
            randomBumper.SetBumper(new Bumper(BumperMoveset[0]));
        }
        EnemyUIManager.Instance.PlayAnimation("Growl");
    }
    
}