﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public abstract class Enemy : MonoBehaviour, IHealth
{
    protected static Enemy _instance;
    public static Enemy Instance => _instance;

    [SerializeField] protected List<EnemyBumper> Bumpers;
    protected Health Health;

    [SerializeField] protected List<BumperDetail> BumperMoveset;
    [SerializeField] protected int AttackTick = 3;
    protected Timer attackTimer = null;

    [SerializeField] private int MaxAttack = 1;

    private void Awake()
    {
        _instance = this;
    }

    protected virtual void Start()
    {
        Health = GetComponent<Health>();
        attackTimer = new Timer(AttackTick);

        UpdateHealthUI();
    }

    protected virtual void Update()
    {
        CheckStun();

        attackTimer.IsActive = CheckBumperActiveLimit();
        AttackBehavior();
        if (Health.Hp == 0)
        {
            FightManager.Instance.OnFightEnd.Invoke();
        }
    }

    protected EnemyBumper GetRandomEmptyBumper()
    {
        EnemyBumper bumper = Bumpers[Random.Range(0, Bumpers.Count)];
        if (bumper.Bumper == null)
        {
            return bumper;
        }
        else
        {
            return GetRandomEmptyBumper();
        }
    }

    protected virtual bool CheckBumperEmpty()
    {
        foreach (var bumper in Bumpers)
        {
            if (bumper.Bumper == null)
            {
                return true;
            }
        }

        return false;
    }

    protected virtual bool CheckBumperActiveLimit()
    {
        int count = 0;

        foreach (var bumper in Bumpers)
        {
            if (bumper.Bumper != null)
            {
                count++;
            }
        }

        return count < MaxAttack;
    }

    private void AttackBehavior()
    {
        if (!attackTimer.IsActive)
        {
            return;
        }

        if (attackTimer.TimerEnd())
        {
            if (CheckBumperActiveLimit())
            {
                Attack();
            }

            attackTimer = new Timer(AttackTick);
        }
    }

    protected abstract void Attack();

    protected virtual void Skill()
    {
    }

    private bool isStunned = false;
    private Timer stunTimer = new Timer(0, false);

    public void Stunned(int stunTick)
    {
        isStunned = true;
        stunTimer = new Timer(stunTick, isStunned);

        attackTimer.IsActive = !isStunned;
        foreach (EnemyBumper b in Bumpers)
        {
            b.SetTimerStatus(!isStunned);
        }

        EnemyUIManager.Instance.SetEffect(EffectType.Stun, isStunned);
    }

    private void CheckStun()
    {
        if (stunTimer.IsActive)
        {
            if (stunTimer.TimerEnd())
            {
                StunEnd();
            }
        }
    }

    private void StunEnd()
    {
        isStunned = false;
        stunTimer.IsActive = isStunned;

        attackTimer.IsActive = !isStunned;
        foreach (EnemyBumper b in Bumpers)
        {
            b.SetTimerStatus(!isStunned);
        }

        EnemyUIManager.Instance.SetEffect(EffectType.Stun, isStunned);
    }

    public virtual void Damage(int amount)
    {
        Health.Damage(amount);
        UpdateHealthUI();
        EnemyUIManager.Instance.PlayAnimation("hurt");
        SfxManager.I.PlayClip("enemyHurt");
    }

    public void Armor(int amount)
    {
        Health.AddArmor(amount);
        UpdateHealthUI();
    }

    public void Heal(int amount)
    {
        Health.Heal(amount);
        UpdateHealthUI();
    }

    private void UpdateHealthUI()
    {
        EnemyUIManager.Instance.Health.UpdateHealth(Health.Hp, Health.HpMax);
        EnemyUIManager.Instance.Health.UpdateArmor(Health.Armor);
    }
}