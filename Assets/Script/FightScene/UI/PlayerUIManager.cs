using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerUIManager : MonoBehaviour
{
    [SerializeField] private HealthUI playerHealth;
    [SerializeField] private PlayerUILoadout _uiLoadout;
    [SerializeField] private Animator PlayerAnimator;
    [SerializeField] private ParticleSystem HealFX;
    [SerializeField] private ParticleSystem AtkBuffFX;
    [SerializeField] private AudioSource audioSource;

    public Animator gameOverUI;
    public Animator gameEndUI;
    public GameObject randomUI;


    public HealthUI Health => playerHealth;
    public PlayerUILoadout Loadout => _uiLoadout;

    private static PlayerUIManager _instantce;

    public static PlayerUIManager Instance
    {
        get => _instantce;
    }

    private void Awake()
    {
        _instantce = this;
    }

    private InventoryDisplayManager _idm;
    public InventoryDisplayManager InventoryDisplay;
    
    private VictoryDisplayManager _vdm;
    public VictoryDisplayManager VictoryDisplay;
    private void Start()
    {
        InventoryDisplay = _idm = GetComponent<InventoryDisplayManager>();
        VictoryDisplay = _vdm  = GetComponent<VictoryDisplayManager>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void PlayAnimation(string name)
    {
        PlayerAnimator.Play(name);
    }

    public void PlayParticleOnce(string name)
    {
        if (name == "Heal")
        {
            HealFX.Play();
        }   
    }
    
    public void ToggleParticle(string name,bool flag)
    {
        if (name == "AtkBuff")
        {
            if (flag)
            {
                AtkBuffFX.Play();
            }
            else
            {
                AtkBuffFX.Stop();
            }

        }   
    }

    public void SceneMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
    public void ToggleRandomUI(bool s)
    {
        randomUI.SetActive(s);
    }
}