﻿using UnityEngine;

public class EffectUI : MonoBehaviour
{
    [SerializeField]private GameObject stunEffect;

    public void StunEffect(bool flag)
    {
        stunEffect.SetActive(flag);
    }

}

public enum EffectType
{
    Stun
}