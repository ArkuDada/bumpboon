using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUIManager : MonoBehaviour
{
    [SerializeField] private EffectUI EffectUI;
    [SerializeField] private Animator EnemyAnimator;
    [SerializeField] private HealthUI enemyHealth;
    [SerializeField] private Image sprite;
    [SerializeField] private Image floor;
    [SerializeField] private Image bg;
    public HealthUI Health => enemyHealth;

    private static EnemyUIManager _instantce;

    public static EnemyUIManager Instance
    {
        get => _instantce;
    }

    private void Awake()
    {
        _instantce = this;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetEffect(EffectType effectType, bool flag)
    {
        switch (effectType)
        {
            case EffectType.Stun:
                EffectUI.StunEffect(flag);
                break;
        }
    }

    public void LoadEnemyUI(EnemyDetailInfo info)
    {
        sprite.sprite = info.EnemyImg;
        floor.sprite = info.EnemyFloor;
        bg.sprite = info.EnemyBG;
    }

    public void PlayAnimation(string name)
    {
        EnemyAnimator.Play(name);
    }
}