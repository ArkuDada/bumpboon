using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BumperUI : MonoBehaviour
{
    [SerializeField] private GameObject detailBoard;
    [SerializeField] private TextMeshPro text;

    public Button button;


    private Bumper _bumper;
    public InvMode mode = InvMode.None;

    private void Start()
    {
        
    }

    public void SetUp(Bumper bumper, InvMode m)
    {
        _bumper = bumper;
        mode = m;
        GetComponent<Image>().sprite = _bumper.Sprite;

        if (m == InvMode.Interact)
        {
            button.interactable = true;
        }
    }

    public void DisplayInfo()
    {
        if (mode >= InvMode.Detail)
        {
            detailBoard.SetActive(true);

            BumperEffectParse effect = _bumper.effect.GetBumperEffectData(_bumper.isUpgraded);
            string construct = "";
            construct += effect.effectName;
            foreach (var data in effect.Datas)
            {
                construct += "\n" + data.name + " : " + data.value.ToString();
            }

            text.text = construct;
        }
    }

    public void HideInfo()
    {
        detailBoard.SetActive(false);
    }
}

[Serializable]
public enum InvMode
{
    None = 0,
    Detail = 1,
    Interact = 2,
    Upgrade = 3
}