﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerUILoadout : MonoBehaviour
{
    [Title("Slot")] [SerializeField] private GameObject firstSlot;
    [SerializeField] private GameObject loadoutSlot;

    private GameObject bumperPrefab;

    private List<GameObject> bumperInLoadout = new List<GameObject>();

    private void Start()
    {
        bumperPrefab = PrefabRef.Instance.BumperUI;
    }

    public void ChangeList(List<Bumper> bList)
    {
        foreach (var bumper in bumperInLoadout)
        {
            Destroy(bumper);
        }

        bumperInLoadout.Clear();
        foreach (var bumper in bList)
        {
            GameObject newBumper = Instantiate(bumperPrefab, loadoutSlot.transform);
            newBumper.GetComponent<BumperUI>().SetUp(bumper,InvMode.None);
            bumperInLoadout.Add(newBumper);
        }

        UpdateUI();
    }

    private void UpdateUI()
    {
        bumperInLoadout[0].transform.SetParent(firstSlot.transform);
        bumperInLoadout[0].GetComponent<RectTransform>().position = firstSlot.GetComponent<RectTransform>().position;

        foreach (var bGameObject in bumperInLoadout.Skip(1))
        {
            bGameObject.transform.SetParent(loadoutSlot.transform);
        }
    }
}