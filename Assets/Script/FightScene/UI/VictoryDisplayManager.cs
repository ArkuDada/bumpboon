using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VictoryDisplayManager : MonoBehaviour
{
    [SerializeField] private GameObject victory;
    [SerializeField] private RectTransform bumperSlot;
    [SerializeField] private TextMeshProUGUI description;
    private Button bumperButton;
    [SerializeField] private Button upgradeButton;
    [SerializeField] private Button nextButton;

    private GameObject bumperPrefab;

    private void Start()
    {
        bumperPrefab = PrefabRef.Instance.BumperUI;

        upgradeButton.onClick.AddListener(OpenUpgradeInv);
    }

    public void LoadReward()
    {
        description.text = "Pick a Reward!";
        LoadRandomBumper();
        upgradeButton.interactable = true;
    }

    private void LoadRandomBumper()
    {
        Bumper bumper = new Bumper(Database.GetRandomBumper());
        GameObject newBumperUI = Instantiate(bumperPrefab, bumperSlot.transform);
        newBumperUI.transform.SetParent(bumperSlot.transform);
        BumperUI ui = newBumperUI.GetComponent<BumperUI>();
        ui.SetUp(bumper, InvMode.Interact);

        bumperButton = ui.button;
        print(bumper.effect.originalBumperDetail.name);


        ui.button.onClick.AddListener(delegate { PlayerManager.Instance.Deck.Add(bumper); });
        //ui.button.onClick.AddListener(delegate { OpenUpgradePanel(bumper); });
        ui.button.onClick.AddListener(delegate { ChoiceSelected(true); });
    }

    //private void
    private void ChoiceSelected(bool flag)
    {
        bumperButton.interactable = false;
        Destroy(bumperButton.gameObject);

        upgradeButton.interactable = false;

        description.text = flag ?"You pick up new Bumper!":"You upgrade your Bumper!";
    }


    public void OpenVictoryPanel()
    {
        victory.SetActive(true);
    }

    public void CloseVictoryPanel()
    {
        victory.SetActive(false);
    }


    [SerializeField] private GameObject upgradePanel;
    [SerializeField] private RectTransform upgradeSlot;
    [SerializeField] private TextMeshProUGUI upgradeDetail;
    [SerializeField] private Button upgradeConfirm;

    public void OpenUpgradeInv()
    {
        PlayerUIManager.Instance.InventoryDisplay.OpenInv((int)InvMode.Upgrade);
    }

    public void ConfirmUpgrade(Bumper b)
    {
        b.UpgradeBumper();
        PlayerUIManager.Instance.InventoryDisplay.CloseInv();
        ChoiceSelected(false);
        CloseUpgradePanel();
    }

    public void OpenUpgradePanel(Bumper b)
    {
        upgradePanel.SetActive(true);

        GameObject newBumperUI = Instantiate(bumperPrefab, upgradeSlot.transform);
        newBumperUI.transform.SetParent(upgradeSlot.transform);
        BumperUI ui = newBumperUI.GetComponent<BumperUI>();
        ui.SetUp(b, InvMode.None);

        var baseData = b.effect.GetBumperEffectData(false);
        var upData = b.effect.GetBumperEffectData(true);
        string construct = "";
        construct += baseData.effectName;
        for (int i = 0; i < baseData.Datas.Count; i++)
        {
            construct += "\n" + baseData.Datas[i].name + " : " + baseData.Datas[i].value + " -> " +
                         upData.Datas[i].value;
        }

        upgradeDetail.text = construct;

        upgradeConfirm.onClick.RemoveAllListeners();
        upgradeConfirm.onClick.AddListener(delegate { ConfirmUpgrade(b); });
    }

    public void CloseUpgradePanel()
    {
        upgradePanel.SetActive(false);
    }
}