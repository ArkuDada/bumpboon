using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    [SerializeField] private GameObject healthIcon;
    [SerializeField] private TextMeshProUGUI healthText;

    [SerializeField] private GameObject shieldIcon;
    [SerializeField] private TextMeshProUGUI shieldText;

    [SerializeField] private Image barFiller;
    [SerializeField] private Sprite hFiller;
    [SerializeField] private Sprite aFiller;

    private int currentHp = 0;
    private int maxHp = 0;

    private int armor = 0;

    public void UpdateHealth(int cur, int max)
    {
        currentHp = cur;
        maxHp = max;

        barFiller.fillAmount = (float)currentHp / maxHp;
        healthText.text = currentHp + " / " + maxHp;
    }

    public void UpdateArmor(int cur)
    {
        armor = cur;
        bool isArmor = armor > 0;
        shieldIcon.SetActive(isArmor);
        barFiller.sprite = isArmor ? aFiller : hFiller;
        if (isArmor)
        {
            shieldText.text = armor.ToString();
        }
    }
}