using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDisplayManager : MonoBehaviour
{
    [SerializeField] private GameObject Inventory;
    [SerializeField] private GameObject ContentParent;

    [SerializeField] private PlayerBumperDeck _deck;

    private GameObject bumperPrefab;

    private List<GameObject> bumperInLoadout = new List<GameObject>();

    void Start()
    {
        _deck = PlayerManager.Instance.Deck;
        bumperPrefab = PrefabRef.Instance.BumperUI;
    }

    private void ChangeList(InvMode mode)
    {
        foreach (var bumper in bumperInLoadout)
        {
            Destroy(bumper);
        }

        bumperInLoadout.Clear();
        foreach (var bumper in _deck.Deck)
        {
            if (mode == InvMode.Upgrade && bumper.isUpgraded)
            {
                continue;
            }
            
            GameObject newBumper = Instantiate(bumperPrefab, ContentParent.transform);;
            
            newBumper.transform.SetParent(ContentParent.transform);
            newBumper.GetComponent<BumperUI>().SetUp(bumper, mode);
            bumperInLoadout.Add(newBumper);

            if (mode == InvMode.Upgrade)
            {
                var b = newBumper.GetComponent<Button>();
                b.interactable = true;
                b.onClick.AddListener(delegate { PlayerUIManager.Instance.VictoryDisplay.OpenUpgradePanel(bumper); });
            }
        }
    }

    private bool isInvOpen = false;
    
    [Button]
    public void ToggleInv(int mode)
    {
        isInvOpen = !isInvOpen;
        if (isInvOpen)
        {
            OpenInv(mode);
        }
        else
        {
            CloseInv();
        }
    }

    public void OpenInv(int mode)
    {
        Inventory.SetActive(true);
        ChangeList((InvMode)mode);
    }

    public void CloseInv()
    {
        Inventory.SetActive(false);
    }
}