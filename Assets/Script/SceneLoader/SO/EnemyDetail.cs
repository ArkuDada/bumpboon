using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Enemy Detail", menuName = "Asset/Enemy/Detail", order = 0)]
public class EnemyDetail : BumpboonSceneAsset
{
    public EnemyDetailInfo info;
}

[Serializable]
public struct EnemyDetailInfo
{
    public Sprite EnemyImg;
    public Sprite EnemyFloor;
    public Sprite EnemyBG;
}