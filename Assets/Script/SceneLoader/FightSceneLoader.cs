using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FightSceneLoader : MonoBehaviour
{
    private static FightSceneLoader _instance;

    public static FightSceneLoader Instance
    {
        get => _instance;
    }

    private EnemyDetail currentScene;

    private void Awake()
    {
        _instance = this;
    }

    public void LoadFight(EnemyDetail fightDetail)
    {
        StartCoroutine(LoadAsycnScene(fightDetail));
    }

    private IEnumerator LoadAsycnScene(EnemyDetail fightDetail)
    {
        currentScene = fightDetail;
        var asyncLoad = SceneManager.LoadSceneAsync(currentScene.scenePath, LoadSceneMode.Additive);
        EnemyUIManager.Instance.LoadEnemyUI(currentScene.info);

        yield return new WaitUntil(() => asyncLoad.isDone);
        
        MapManager.i.ToggleVisible(false);
        FightManager.Instance.FightStart();
    }

    public void UnloadFight()
    {
        SceneManager.UnloadSceneAsync(currentScene.scenePath);
        currentScene = null;
        FightManager.Instance.State = FightState.Unload;
    }

    [SerializeField] private EnemyDetail debugFight;

    [Button]
    public void LoadDebugFight()
    {
        LoadFight(debugFight);
    }
}