using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BumpboonSceneLoader : MonoBehaviour
{
    [SerializeField] private GameScene Menu;
    [SerializeField] private GameScene FightManager;

    private void Start()
    {
        LoadFightManager();
    }

    public void LoadFightManager()
    {
        SceneManager.LoadSceneAsync(FightManager.scenePath, LoadSceneMode.Additive);
    }

    public void UnloadFightManager()
    {
        SceneManager.UnloadSceneAsync(FightManager.scenePath);
    }
}