﻿using UnityEngine;

[CreateAssetMenu(fileName = "BasicAttack", menuName = "Asset/Bumper/Enemy/Basic Attack Bumper", order = 0)]
public class BasicEnemyAttack : BumperDetail<BasicEnemyAttackData, BasicEnemyAttackEffect>
{
}

public class BasicEnemyAttackEffect : BumperEffect<BasicEnemyAttackData>
{
    private BasicEnemyAttackData CurrentData;
    private BasicEnemyAttackData _baseStat;
    private BasicEnemyAttackData _upgradeStat;

    private EnemyBumper assignedBumper;

    public override void Init(BasicEnemyAttackData bs, BasicEnemyAttackData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }
    public override void Upgrade()
    {
        throw new System.NotImplementedException();
    }
    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        BasicEnemyAttackData selectData = getUpgradeData ? _upgradeStat : _baseStat;
        BumperEffectParse parse = new BumperEffectParse("Enemy Bumper");
        //parse.AddData("Heal", selectData.heal);

        return parse;
    }


    public override void Activate()
    {
        PlayerManager.Instance.Damage(CurrentData.Damage);
        EnemyUIManager.Instance.PlayAnimation("Tackle");
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        assignedBumper = bumper as EnemyBumper;

        if (assignedBumper is { })
        {
            assignedBumper.BumperHealth = CurrentData.Health;
            assignedBumper.AttackTick = CurrentData.Timer;
        }
    }

    public override void OnHit()
    {
    }
}

[System.Serializable]
public struct BasicEnemyAttackData
{
    [SerializeField] private int hitToBreak;
    public int Health => hitToBreak;
    
    [SerializeField] private int damagePerAttack;
    public int Damage => damagePerAttack;

    [SerializeField] private int tickPerAttack;
    public int Timer => tickPerAttack;
}