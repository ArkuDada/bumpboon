﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scatter Bumper", menuName = "Asset/Bumper/Player/Scatter Bumper", order = 5)]
public class ScatterBumper : BumperDetail<ScatterBumperData, ScatterBumperEffect>
{
}

public class ScatterBumperEffect : BumperEffect<ScatterBumperData>
{
    private ScatterBumperData CurrentData;
    private ScatterBumperData _baseStat;
    private ScatterBumperData _upgradeStat;

    public override void Init(ScatterBumperData bs, ScatterBumperData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }
    public override void Upgrade()
    {
        bumperName += "+";
        CurrentData = _upgradeStat;
    }

    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        ScatterBumperData selectData = getUpgradeData ? _upgradeStat : _baseStat;
        BumperEffectParse parse = new BumperEffectParse(bumperName);
        parse.AddData("Split", selectData.amount);

        return parse;
    }

    public override void Activate()
    {
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        throw new System.NotImplementedException();
    }

    public override void OnHit()
    {
        for (int i = 0; i < CurrentData.amount; i++)
        {
            BallManager.Instance.SpawnBall(true);
        }
    }
}

[System.Serializable]
public struct ScatterBumperData
{
    [SerializeField] private int scatterAmount;
    public int amount => scatterAmount;
}