﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack Bumper", menuName = "Asset/Bumper/Player/Attack Bumper", order = 0)]
public class AttackBumper : BumperDetail<AttackBumperData, AttackBumperEffect>
{
}

public class AttackBumperEffect : BumperEffect<AttackBumperData>
{
    private AttackBumperData CurrentData;
    private AttackBumperData _baseStat;
    private AttackBumperData _upgradeStat;

    public override void Init(AttackBumperData bs, AttackBumperData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }

    public override void Upgrade()
    {
        bumperName += "+";
        CurrentData = _upgradeStat;
    }

    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        AttackBumperData selectData = getUpgradeData ? _upgradeStat : _baseStat;
        BumperEffectParse parse = new BumperEffectParse(bumperName);
        parse.AddData("Damage", selectData.damage);

        return parse;
    }

    public override void Activate()
    {
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        throw new System.NotImplementedException();
    }


    public override void OnHit()
    {
        PlayerBrain.Instance.AttackBuffer(CurrentData.damage);
        PlayerBrain.Instance.CommenceAttack(AttackAnimEnum.Attack);
    }
}

[System.Serializable]
public struct AttackBumperData
{
    [SerializeField] private int damageOnHit;
    public int damage => damageOnHit;
}