﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Stun Bumper", menuName = "Asset/Bumper/Player/Stun Bumper", order = 6)]
public class StunBumper : BumperDetail<StunBumperData, StunBumperEffect>
{
}

public class StunBumperEffect : BumperEffect<StunBumperData>
{
    private StunBumperData CurrentData;
    private StunBumperData _baseStat;
    private StunBumperData _upgradeStat;

    public override void Init(StunBumperData bs, StunBumperData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }

    public override void Upgrade()
    {
        bumperName += "+";
        CurrentData = _upgradeStat;
    }

    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        StunBumperData selectData = getUpgradeData ? _upgradeStat : _baseStat;
        BumperEffectParse parse = new BumperEffectParse(bumperName);
        parse.AddData("Damage", selectData.damage);
        parse.AddData("Duration", selectData.stun);

        return parse;
    }

    public override void Activate()
    {
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        throw new System.NotImplementedException();
    }


    public override void OnHit()
    {
        PlayerBrain.Instance.AttackBuffer(CurrentData.damage);
        PlayerBrain.Instance.CommenceAttack(AttackAnimEnum.AttackStone);

        Enemy.Instance.Stunned(CurrentData.stun);
    }
}

[System.Serializable]
public struct StunBumperData
{
    [SerializeField] private int damageOnHit;
    [SerializeField] private int stunDuration;
    public int damage => damageOnHit;
    public int stun => stunDuration;
}