﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack Buff Bumper", menuName = "Asset/Bumper/Player/Attack Buff Bumper", order = 4)]
public class AttackBuffBumper : BumperDetail<AttackBuffBumperData, AttackBuffBumperEffect>
{
}

public class AttackBuffBumperEffect : BumperEffect<AttackBuffBumperData>
{
    private AttackBuffBumperData CurrentData;
    private AttackBuffBumperData _baseStat;
    private AttackBuffBumperData _upgradeStat;

    public override void Init(AttackBuffBumperData bs, AttackBuffBumperData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }

    public override void Upgrade()
    {
        bumperName += "+";
        CurrentData = _upgradeStat;
    }

    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        AttackBuffBumperData selectData = getUpgradeData ? _upgradeStat : _baseStat;
        BumperEffectParse parse = new BumperEffectParse(bumperName);
        parse.AddData("Attack * ", selectData.Buff);

        return parse;
    }

    public override void Activate()
    {
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        throw new System.NotImplementedException();
    }


    public override void OnHit()
    {
        PlayerBrain.Instance.AttackMultiplier(CurrentData.Buff);
    }
}

[System.Serializable]
public struct AttackBuffBumperData
{
    [SerializeField] private int buffAmount;
    public int Buff => buffAmount;
}