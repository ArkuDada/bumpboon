using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shield Bumper", menuName = "Asset/Bumper/Player/Shield Bumper", order = 1)]
public class ShieldBumper : BumperDetail<ShieldBumperData, ShieldBumperEffect>
{
}

public class ShieldBumperEffect : BumperEffect<ShieldBumperData>
{
    private ShieldBumperData CurrentData;
    private ShieldBumperData _baseStat;
    private ShieldBumperData _upgradeStat;

    public override void Init(ShieldBumperData bs, ShieldBumperData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }

    public override void Upgrade()
    {
        bumperName += "+";
        CurrentData = _upgradeStat;
    }

    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        ShieldBumperData selectData = getUpgradeData ? _upgradeStat : _baseStat;

        BumperEffectParse parse = new BumperEffectParse(bumperName);
        parse.AddData("Armor", selectData.armor);

        return parse;
    }

    public override void Activate()
    {
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        throw new System.NotImplementedException();
    }


    public override void OnHit()
    {
        PlayerManager.Instance.Armor(CurrentData.armor);
    }
}

[System.Serializable]
public struct ShieldBumperData
{
    [SerializeField] private int armorAmount;
    public int armor => armorAmount;
}