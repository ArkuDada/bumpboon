﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Heal Bumper", menuName = "Asset/Bumper/Player/Heal Bumper", order = 1)]
public class HealBumper : BumperDetail<HealBumperData, HealBumperEffect>
{
}

public class HealBumperEffect : BumperEffect<HealBumperData>
{
    private HealBumperData CurrentData;
    private HealBumperData _baseStat;
    private HealBumperData _upgradeStat;

    public override void Init(HealBumperData bs, HealBumperData us)
    {
        base.Init(bs, us);
        _baseStat = bs;
        _upgradeStat = us;

        CurrentData = _baseStat;
    }

    public override void Upgrade()
    {
        bumperName += "+";
        CurrentData = _upgradeStat;
    }

    public override BumperEffectParse GetBumperEffectData(bool getUpgradeData)
    {
        HealBumperData selectData = getUpgradeData ? _upgradeStat : _baseStat;
        BumperEffectParse parse = new BumperEffectParse(bumperName);
        parse.AddData("Heal", selectData.heal);

        return parse;
    }

    public override void Activate()
    {
    }

    public override void OnApplied<TAppliedBumper>(TAppliedBumper bumper)
    {
        throw new System.NotImplementedException();
    }


    public override void OnHit()
    {
        PlayerManager.Instance.Heal(CurrentData.heal);
    }
}

[System.Serializable]
public struct HealBumperData
{
    [SerializeField] private int healAmount;
    public int heal => healAmount;
}