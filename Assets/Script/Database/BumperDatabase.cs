﻿using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Bumper Database", menuName = "Asset/Databases/Bumper Database", order = 1)]
public class BumperDatabase : ScriptableObject
{
    public List<BumperDetail> allBumpers;
    public int Count => allBumpers.Count;
}