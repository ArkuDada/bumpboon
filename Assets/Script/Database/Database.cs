﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Random = UnityEngine.Random;


public class Database : MonoBehaviour
{
    public BumperDatabase bumpers;
    public BumperDatabase starterDeck;
    public BumperDatabase debugDeck;

    private static Database instance;

    private void Awake()
    {
            instance = this;
        
    }

    public static BumperDetail GetBumperByInt(int i)
    {
        return instance.bumpers.allBumpers[i];
    }

    public static BumperDetail GetBumperByID(string ID)
    {
        return instance.bumpers.allBumpers.FirstOrDefault(i => i.ID == ID);
    }

    public static BumperDetail GetRandomBumper()
    {
        return instance.bumpers.allBumpers[Random.Range(0, instance.bumpers.allBumpers.Count)];
    }

    public static List<BumperDetail> GetDebugDeck()
    {
        return instance.debugDeck.allBumpers;
    }

    public static List<BumperDetail> GetStarterDeck()
    {
        return instance.starterDeck.allBumpers;
    }
}