﻿using System.Collections.Generic;
using UnityEngine;

public abstract class BumperEffect
{
    public BumperDetail originalBumperDetail;
    public abstract void Activate();
    public abstract void OnApplied<TAppliedBumper>(TAppliedBumper bumper);
    public abstract void OnHit();

    public abstract void Upgrade();
    public abstract BumperEffectParse GetBumperEffectData(bool getUpgradeData);
}

public abstract class BumperEffect<DataType> : BumperEffect where DataType : struct
{
    protected string bumperName;
    private DataType _baseStat;
    private DataType _upgradeStat;

    public virtual void Init(DataType bs, DataType us)
    {
        _baseStat = bs;
        _upgradeStat = us;
    }

    public virtual void SetName(string n)
    {
        bumperName = n;
    }
}

public class BumperEffectParse
{
    public string effectName;
    public List<BumperFieldData> Datas;

    public struct BumperFieldData
    {
        public string name;
        public int value;
    }

    public BumperEffectParse(string en)
    {
        effectName = en;
        Datas = new List<BumperFieldData>();
    }

    public void AddData(string n, int v)
    {
        BumperFieldData newBumperFieldData;
        newBumperFieldData.name = n;
        newBumperFieldData.value = v;
        
        Datas.Add(newBumperFieldData);
    }
}