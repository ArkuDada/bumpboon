﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class BumperDetail : ScriptableObject
{
    [SerializeField] protected string BumperID;
    public string ID => BumperID;

    [SerializeField] protected Sprite _sprite;

    public Sprite Sprite => _sprite;
    public abstract BumperEffect CreateBumper();
}

public abstract class BumperDetail<DataType, EffectType> : BumperDetail where DataType : struct
    where EffectType : BumperEffect<DataType>, new()
{
    [SerializeField] private string bumperName;
    [SerializeField] private DataType baseStat;
    [SerializeField] private DataType upgradeStat;

    public override BumperEffect CreateBumper()
    {
        var effect = new EffectType()
        {
            originalBumperDetail = this
        };

        effect.Init(baseStat, upgradeStat);
        effect.SetName(bumperName);
        
        return effect;
    }
}